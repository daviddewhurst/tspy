#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

import nonparametric


def main():
    N = 100000
    time = np.linspace(1, N, N)
    filt = np.linspace(-10, 10, 5000)
    filt = filt**2
    filt /= len(filt)

    x = np.correlate( np.random.randn(N), filt, mode='same' ) 

    xcorr = nonparametric.autocorr(x)
    xcorr_fft = nonparametric.autocorr(x, mode='fft')
    
    fig, axes = plt.subplots(2, 1, figsize=(10, 10))
    ax, ax2 = axes
    ax.plot(time, x)
    ax2.plot(xcorr, label='time-based')
    ax2.plot(xcorr_fft, label='freq-based')
    ax2.legend()

    plt.show()


if __name__ == "__main__":

    main()
