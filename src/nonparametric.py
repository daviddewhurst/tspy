#!/usr/bin/env python

import numpy as np
from scipy import stats


def cross_psd(x, y, d=1.0, log=False):
    """
    Defines cross-spectral density
    """
    assert x.shape[0] == y.shape[0]
    const = 1.0 / len(x)
    N = len(x)
    print(N)
    
    pxy = const * np.absolute( np.fft.rfft(x) * np.conjugate( np.fft.rfft(y) ) )
    f = np.fft.fftfreq(x.shape[0], d=d)
    
    # we don't need the double-sided transform since we don't care about negative freq
    if N % 2 == 0:
        cut = int(N/2)
        f = f[:cut]
        pxy = pxy[:cut]
    else:
        cut = int((N - 1)/2)
        f = f[:cut]
        pxy = pxy[:cut]
        
    if not log:
        return f, pxy
    
    # estimate the psd exponent
    logf = np.log10( f[1:] )
    logpxy = np.log10( pxy[1:] )
    gamma, _, _, _, _ = stats.linregress(logf, logpxy)
    return logf, logpxy, gamma


def psd(x, d=1.0, log=False):
    return cross_psd(x, x, d=d, log=log)


def crosscorr(x, y, mode='conv'):
    """
    """
    assert len(x) == len(y)
    const = len(x) * np.std(x) * np.std(y)
    x = x - np.mean(x)
    y = y - np.mean(y)
    if mode == 'conv':
        return 1.0 / const * np.correlate(x, y, 'full')
    elif mode == 'fft':
        xhat = np.fft.fft(x)
        yhat = np.fft.fft(y)
        pxy = xhat * np.conj(yhat)
        xcorr = np.fft.ifft(pxy) / const
        return np.concatenate( (xcorr, xcorr[::-1][1:]) )


def autocorr(x, mode='conv'):
    return crosscorr(x, x, mode=mode)


def cepstrum(x):
    """
    """
    n = len(x)
    if n % 2 == 0:
        N = int(n / 2)
    else:
        N = int((n - 1) / 2)

    fx = np.fft.rfft( x, norm='ortho' )
    abs_fx = np.log10(  np.absolute(fx) )
    cx = np.fft.irfft(abs_fx, norm='ortho')

    return cx[:N]


def power_ceptrum(x):
    """
    """
    n = len(x)
    if n % 2 == 0:
        N = int(n / 2)
    else:
        N = int((n - 1) / 2)
    
    fx = np.fft.rfft( x, norm='ortho' )
    abs_fx = np.absolute(fx)
    cxx = np.absolute(
            np.fft.irfft(
                np.log10(abs_fx**2), norm='ortho'
                )
            )

    return cxx[:N]


def cummean(x):
    return np.cumsum(x) / np.linspace(1, len(x), len(x))
